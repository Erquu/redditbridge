<?php

function include_image($mime_type, $media_type, $sub_type, $data) {
  switch ($sub_type) {
    case 'png':
    case 'jpeg':
    case 'png':
    case 'gif':
      header("Content-type: $mime_type");
      echo $data;
      return true;
    default:
      echo "$sub_type images are not allowed";
      break;
  }
  return false;
}

function include_text($mime_type, $media_type, $sub_type, $data) {
  switch ($sub_type) {
    case 'css':
    case 'html':
    case 'plain':
    case 'xml':
      header("Content-type: $mime_type");
      echo $data;
      return true;
    default:
      echo "$sub_type text files are not allowed";
      break;
  }
}


$url = $_SERVER['REQUEST_URI'];
$pattern = '/\?(.*)$/';

if (preg_match($pattern, $url, $capture, PREG_OFFSET_CAPTURE) === 1) {
  if (sizeof($capture) === 2) {
    $fc = $capture[1];
    if (sizeof($fc) === 2) {
    	$url = base64_decode($fc[0]);

    	$opts = array(
  			'http'=>array(
  				'method'=>"GET",
  				'header'=>"Accept-language: en\r\n"
  			)
  		);

  		$context = stream_context_create($opts);
    	
    	$data = file_get_contents($url, false, $context);
      $file_info = new finfo(FILEINFO_MIME_TYPE);
      $mime_type = $file_info->buffer($data);

      $debug = $url . ";" . $mime_type . "\r\n";

      $pattern = '/(text|image|video|audio|application|multipart|message|model)\/(.*)/i';

      if (preg_match($pattern, $mime_type, $capture, PREG_OFFSET_CAPTURE) === 1) {
        // Debug stuff
        ob_start();
        print_r($capture);
        $db_capture = ob_get_contents();
        ob_end_clean();
        $debug .= $db_capture . "\r\n";
        // End debug stuff

        $handler = array(
          'image' => 'include_image',
          'text' => 'include_text'
        );

        if (sizeof($capture) === 3) {
          $media_type = $capture[1][0];
          $sub_type = $capture[2][0];

          if (array_key_exists($media_type, $handler)) {
            call_user_func_array($handler[$media_type], array(
              $mime_type,
              $media_type,
              $sub_type,
              $data
            ));
          } else {
            echo "Files of type $mime_type are not allowed!";
          }
        }
      } else {
        echo "Invalid mime type";
      }

      file_put_contents('bridge.log', $debug, FILE_APPEND);
    }
  }
}

?>
